//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.dts.spell.finder;

public interface WordFinder {
  Word current();

  boolean hasNext();

  Word next();

  void replace(String var1);
}
