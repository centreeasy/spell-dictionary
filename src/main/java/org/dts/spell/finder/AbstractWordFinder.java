//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.dts.spell.finder;

public abstract class AbstractWordFinder implements WordFinder {
  private Word currentWord = null;
  private Word nextWord = null;

  public AbstractWordFinder() {
  }

  protected abstract Word next(Word var1);

  public Word next() {
    if (this.nextWord == null) {
      throw new WordNotFoundException();
    } else {
      this.currentWord = this.nextWord;
      this.nextWord = this.next(this.currentWord);
      return this.currentWord;
    }
  }

  public Word current() {
    if (this.currentWord == null) {
      throw new WordNotFoundException();
    } else {
      return this.currentWord;
    }
  }

  public boolean hasCurrent() {
    return this.currentWord != null;
  }

  public boolean hasNext() {
    return this.nextWord != null;
  }

  protected abstract void replace(String var1, Word var2);

  public void replace(String newWord) {
    if (this.currentWord == null) {
      throw new WordNotFoundException();
    } else {
      boolean isStart = this.currentWord.isStartOfSentence();
      if (isStart) {
        newWord = Word.getStartSentenceWordCase(newWord);
      }

      this.replace(newWord, this.currentWord);
      this.currentWord = new Word(newWord, this.currentWord.getStart(), isStart);
      this.nextWord = this.next(this.currentWord);
    }
  }

  public void init() {
    this.init((Word) null);
  }

  public void init(Word initWord) {
    this.currentWord = initWord;
    this.nextWord = initWord;
    if (initWord == null) {
      this.nextWord = this.next(initWord);
    }

  }
}
