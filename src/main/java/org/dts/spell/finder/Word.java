//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.dts.spell.finder;

public class Word implements CharSequence {
  private int start;
  private String text;
  private boolean isStart;

  public Word(String text, int start, boolean isStart) {
    this.text = text;
    this.start = start;
    this.isStart = isStart;
  }

  public Word(String text, int start) {
    this(text, start, false);
  }

  public static String getStartSentenceWordCase(CharSequence word) {
    StringBuilder str = new StringBuilder(word);
    str.setCharAt(0, Character.toUpperCase(word.charAt(0)));
    return str.toString();
  }

  public int getEnd() {
    return this.getStart() + this.length();
  }

  public int getStart() {
    return this.start;
  }

  public String getText() {
    return this.text;
  }

  public int length() {
    return this.text.length();
  }

  public String toString() {
    return this.text;
  }

  public boolean isStartOfSentence() {
    return this.isStart;
  }

  public void setStartStartOfSentence(boolean isStart) {
    this.isStart = isStart;
  }

  public boolean isCorrectFirstChar() {
    char c = this.text.charAt(0);
    return Character.isLetter(c) ? (this.isStartOfSentence() ? Character.isUpperCase(c) : Character.isLowerCase(c)) : true;
  }

  public boolean isUpperCase() {
    return this.text.toUpperCase().equals(this.text);
  }

  public boolean equals(Object o) {
    if (o != null && o instanceof Word) {
      Word ow = (Word) o;
      return ow.getStart() == this.getStart() && ow.getText().equals(this.getText());
    } else {
      return false;
    }
  }

  public char charAt(int index) {
    return this.text.charAt(index);
  }

  public CharSequence subSequence(int start, int end) {
    return this.text.subSequence(start, end);
  }

  public String getStartSentenceWordCase() {
    return getStartSentenceWordCase(this);
  }
}
