//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.dts.spell.finder;

import org.dts.spell.finder.CharSequenceWordFinder;
import org.dts.spell.finder.Word;
import org.dts.spell.tokenizer.WordTokenizer;

public class StringBuilderWordFinder extends CharSequenceWordFinder {
  public StringBuilderWordFinder(StringBuffer text, WordTokenizer tokenizer) {
    super(text, tokenizer);
  }

  public StringBuilderWordFinder(StringBuffer text) {
    super(text);
  }

  protected void replace(String newWord, Word currentWord) {
    int start = currentWord.getStart();
    int oldEnd = currentWord.getEnd();
    int newEnd = newWord.length();
    this.getStringBuilder().replace(start, oldEnd, newWord);
    this.updateCharSequence(start, oldEnd, 1);
    if (newEnd > 0) {
      this.updateCharSequence(start, newEnd, 0);
    }

  }

  public StringBuilder getStringBuilder() {
    return (StringBuilder) this.getCharSequence();
  }
}
