//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.dts.spell.finder;

import org.dts.spell.finder.AbstractWordFinder;
import org.dts.spell.finder.Word;
import org.dts.spell.tokenizer.DefaultWordTokenizer;
import org.dts.spell.tokenizer.WordTokenizer;

public class CharSequenceWordFinder extends AbstractWordFinder {
  private WordTokenizer tokenizer;

  public CharSequenceWordFinder(CharSequence text, WordTokenizer tokenizer) {
    tokenizer.setCharSequence(text);
    this.setTokenizer(tokenizer);
  }

  public CharSequenceWordFinder(CharSequence text) {
    this(text, new DefaultWordTokenizer());
  }

  protected Word next(Word currentWord) {
    Word word;
    if (currentWord == null) {
      byte last = 0;
      word = this.getTokenizer().currentWord(last);
    } else {
      int last1 = currentWord.getEnd();
      word = this.getTokenizer().nextWord(last1 - 1);
    }

    return word;
  }

  protected void replace(String newWord, Word currentWord) {
    throw new UnsupportedOperationException();
  }

  public WordTokenizer getTokenizer() {
    return this.tokenizer;
  }

  protected void setTokenizer(WordTokenizer tokenizer) {
    if (this.getTokenizer() != tokenizer) {
      if (this.getTokenizer() != null) {
        tokenizer.setCharSequence(this.getTokenizer().getCharSequence());
      }

      this.tokenizer = tokenizer;
      this.init();
    }

  }

  protected void updateCharSequence(int start, int end, int cause) {
    this.getTokenizer().updateCharSequence(start, end, cause);
  }

  public CharSequence getCharSequence() {
    return this.getTokenizer() != null ? this.getTokenizer().getCharSequence() : null;
  }
}
