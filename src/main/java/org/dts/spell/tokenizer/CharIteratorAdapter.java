//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.dts.spell.tokenizer;

import java.text.CharacterIterator;
import java.text.MessageFormat;
import java.util.ResourceBundle;

public class CharIteratorAdapter implements CharacterIterator {
  private CharSequence sequence;
  int index;

  public CharIteratorAdapter() {
    this("");
  }

  public CharIteratorAdapter(CharSequence sequence) {
    this.index = 0;
    this.sequence = sequence;
  }

  public char first() {
    this.index = this.getBeginIndex();
    return this.current();
  }

  public char last() {
    if(this.sequence.length() == 0) {
      this.index = this.getEndIndex();
    } else {
      this.index = this.getEndIndex() - 1;
    }

    return this.current();
  }

  public char current() {
    return this.sequence.length() > this.index?this.sequence.charAt(this.index):'\uffff';
  }

  public char next() {
    int endIndex = this.getEndIndex();
    ++this.index;
    char result;
    if(this.index >= endIndex) {
      this.index = endIndex;
      result = '\uffff';
    } else {
      result = this.sequence.charAt(this.index);
    }

    return result;
  }

  public char previous() {
    int beginIndex = this.getBeginIndex();
    --this.index;
    char result;
    if(this.index <= beginIndex) {
      this.index = beginIndex;
      result = '\uffff';
    } else {
      result = this.sequence.charAt(this.index);
    }

    return result;
  }

  public char setIndex(int position) {
    if(position >= this.getBeginIndex() && position <= this.getEndIndex()) {
      this.index = position;
      return this.current();
    } else {
      throw new IllegalArgumentException(MessageFormat.format(ResourceBundle.getBundle("org/dts/spell/messages").getString("INVALID_POSITION"), new Object[]{Integer.valueOf(position), Integer.valueOf(this.getBeginIndex()), Integer.valueOf(this.getEndIndex())}));
    }
  }

  public int getBeginIndex() {
    return 0;
  }

  public int getEndIndex() {
    return this.sequence.length();
  }

  public int getIndex() {
    return this.index;
  }

  public Object clone() {
    try {
      return super.clone();
    } catch (CloneNotSupportedException var2) {
      var2.printStackTrace();
      return null;
    }
  }

  public CharSequence getCharSequence() {
    return this.sequence;
  }

  public void setCharSequence(CharSequence sequence) {
    this.setCharSequence(sequence, true);
  }

  public void setCharSequence(CharSequence sequence, boolean resetIndex) {
    this.sequence = sequence;
    if(resetIndex) {
      this.index = 0;
    }

  }
}
