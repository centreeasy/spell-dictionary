//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.dts.spell.tokenizer;

import org.dts.spell.finder.Word;

public interface WordTokenizer {
  int INSERT_CHARS = 0;
  int DELETE_CHARS = 1;
  int CHANGE_SEQUENCE = 2;

  Word nextWord(int var1);

  Word currentWord(int var1);

  Word previousWord(int var1);

  CharSequence getCharSequence();

  void setCharSequence(CharSequence var1);

  void updateCharSequence(int var1, int var2, int var3);
}
