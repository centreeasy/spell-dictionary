//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.dts.spell.tokenizer;

import org.dts.spell.finder.Word;

public abstract class AbstractWordTokenizer implements WordTokenizer {
  private CharSequence charSequence = "";

  public AbstractWordTokenizer() {
  }

  public abstract Word currentWord(int var1);

  public Word nextWord(int index) {
    int length = this.getCharSequence().length();
    Word orgWord = this.currentWord(index);
    Word current = orgWord;

    Word result;
    for(result = null; index < length && current == null; current = this.currentWord(index)) {
      ++index;
    }

    if(index < length && current != null) {
      if(current.equals(orgWord)) {
        index = orgWord.getEnd();

        while(true) {
          while(index < length && result == null) {
            if(current != null && !current.equals(orgWord)) {
              result = current;
            } else {
              ++index;
              current = this.currentWord(index);
            }
          }

          return result;
        }
      } else {
        result = current;
      }
    }

    return result;
  }

  public Word previousWord(int index) {
    Word orgWord = this.currentWord(index);
    Word current = orgWord;

    Word result;
    for(result = null; index > 0 && current == null; current = this.currentWord(index)) {
      --index;
    }

    if(index > 0 && current != null) {
      if(current.equals(orgWord)) {
        index = orgWord.getStart();

        while(true) {
          while(index > 0 && result == null) {
            if(current != null && !current.equals(orgWord)) {
              result = current;
            } else {
              --index;
              current = this.currentWord(index);
            }
          }

          return result;
        }
      } else {
        result = current;
      }
    }

    return result;
  }

  public CharSequence getCharSequence() {
    return this.charSequence;
  }

  public void setCharSequence(CharSequence sequence) {
    if(sequence != this.charSequence) {
      this.charSequence = sequence;
      this.updateCharSequence(0, sequence != null?sequence.length():0, 2);
    }

  }

  protected boolean isStartOfSentence(CharSequence sequence, int start) {
    boolean found = false;
    --start;

    while(start >= 0 && !found) {
      if(Character.isWhitespace(sequence.charAt(start))) {
        --start;
      } else {
        found = true;
      }
    }

    return start < 0 || sequence.charAt(start) == 46;
  }
}
