//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.dts.spell.tokenizer;

import java.text.BreakIterator;
import org.dts.spell.finder.Word;
import org.dts.spell.tokenizer.AbstractWordTokenizer;
import org.dts.spell.tokenizer.CharIteratorAdapter;

public class DefaultWordTokenizer extends AbstractWordTokenizer {
  private BreakIterator wordIterator;
  private CharIteratorAdapter wordSequence;

  public DefaultWordTokenizer() {
    this(BreakIterator.getWordInstance());
  }

  public DefaultWordTokenizer(BreakIterator wordIterator) {
    this.wordSequence = new CharIteratorAdapter();
    this.wordIterator = wordIterator;
  }

  protected Word scanBefore(CharSequence sequence, int index) {
    int start = this.wordIterator.preceding(index);
    int end = this.wordIterator.next();
    if(start == -1) {
      start = 0;
    }

    String text = sequence.subSequence(start, end).toString().trim();
    return !text.equals("")?new Word(text, start, this.isStartOfSentence(sequence, start)):null;
  }

  protected Word scanAfter(CharSequence sequence, int index) {
    int end = this.wordIterator.following(index);
    int start = this.wordIterator.previous();
    if(end == -1) {
      end = sequence.length() - 1;
    }

    String text = sequence.subSequence(start, end).toString().trim();
    return !text.equals("")?new Word(text, start, this.isStartOfSentence(sequence, start)):null;
  }

  public Word currentWord(int index) {
    CharSequence sequence = this.getCharSequence();
    int length = sequence.length();
    Word result = null;
    if(length > 0 && length >= index) {
      if(index == length) {
        result = this.scanBefore(sequence, index - 1);
      } else if(this.wordIterator.isBoundary(index) && Character.isWhitespace(sequence.charAt(index))) {
        result = this.scanBefore(sequence, index);
      } else {
        result = this.scanAfter(sequence, index);
      }
    }

    return result;
  }

  private void onChangeSequence() {
    CharSequence sequence = this.getCharSequence();
    this.wordSequence.setCharSequence(sequence);
    this.wordIterator.setText(this.wordSequence);
  }

  private void onInsertChars(int start, int end) {
    this.onChangeSequence();
  }

  private void onDeleteChars(int start, int end) {
    this.onChangeSequence();
  }

  public void updateCharSequence(int start, int end, int cause) {
    switch(cause) {
      case 0:
        this.onInsertChars(start, end);
        break;
      case 1:
        this.onDeleteChars(start, end);
        break;
      case 2:
        this.onChangeSequence();
    }

  }
}
