//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.dts.spell.tokenizer;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.dts.spell.finder.Word;
import org.dts.spell.tokenizer.AbstractWordTokenizer;

public class MatcherWordTokenizer extends AbstractWordTokenizer {
  public static final String SPACE_CHARS = "\\s";
  private Matcher matcher;

  public MatcherWordTokenizer(Matcher matcher) {
    this.matcher = matcher;
  }

  public MatcherWordTokenizer() {
    this(Pattern.compile("[^\\s]+").matcher(""));
  }

  public static MatcherWordTokenizer createMatcher(String regexp, int flags) {
    return new MatcherWordTokenizer(Pattern.compile(regexp, flags).matcher(""));
  }

  public static MatcherWordTokenizer createMatcher(String regexp) {
    return new MatcherWordTokenizer(Pattern.compile(regexp).matcher(""));
  }

  public static MatcherWordTokenizer createExcludeMatcher(String chars) {
    return new MatcherWordTokenizer(Pattern.compile("[^" + chars + "]+").matcher(""));
  }

  protected Matcher getMatcher() {
    return this.matcher;
  }

  public Word nextWord(int index) {
    if(!this.matcher.find(index)) {
      return null;
    } else {
      int start = this.matcher.start();
      String text = this.matcher.group();
      return new Word(text, start, this.isStartOfSentence(this.getCharSequence(), start));
    }
  }

  public Word currentWord(int index) {
    if(!this.matcher.find(index)) {
      return null;
    } else {
      int start = this.matcher.start();
      String text = this.matcher.group();
      --index;

      while(index >= 0 && this.matcher.find(index) && start != this.matcher.start()) {
        start = this.matcher.start();
        text = this.matcher.group();
        --index;
      }

      return new Word(text, start, this.isStartOfSentence(this.getCharSequence(), start));
    }
  }

  public void updateCharSequence(int start, int end, int cause) {
    this.getMatcher().reset(this.getCharSequence());
  }
}
