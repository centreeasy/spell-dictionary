//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.dts.spell.event;

public class SpellCheckAdapter implements SpellCheckListener {
  public SpellCheckAdapter() {
  }

  public void beginChecking(SpellCheckEvent event) {
  }

  public void spellingError(SpellCheckEvent event) {
  }

  public void endChecking(SpellCheckEvent event) {
  }

  public void badCaseError(SpellCheckEvent event) {
  }

  public void repeatWordError(SpellCheckEvent event) {
  }
}
