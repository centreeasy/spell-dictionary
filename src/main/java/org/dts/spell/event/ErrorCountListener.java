//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.dts.spell.event;

public class ErrorCountListener extends SpellCheckAdapter {
  private int nErrors;

  public ErrorCountListener() {
  }

  public void beginChecking(SpellCheckEvent event) {
    this.nErrors = 0;
  }

  public void spellingError(SpellCheckEvent event) {
    ++this.nErrors;
  }

  public int getErrorsCount() {
    return this.nErrors;
  }

  public void badCaseError(SpellCheckEvent event) {
    ++this.nErrors;
  }

  public void repeatWordError(SpellCheckEvent event) {
    ++this.nErrors;
  }
}
