//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.dts.spell.event;

import java.util.EventListener;

public interface SpellCheckListener extends EventListener {
  void beginChecking(SpellCheckEvent var1);

  void spellingError(SpellCheckEvent var1);

  void badCaseError(SpellCheckEvent var1);

  void repeatWordError(SpellCheckEvent var1);

  void endChecking(SpellCheckEvent var1);
}
