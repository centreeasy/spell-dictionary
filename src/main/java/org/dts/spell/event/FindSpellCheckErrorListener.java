//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.dts.spell.event;

public class FindSpellCheckErrorListener extends SpellCheckAdapter {
  private org.dts.spell.finder.Word badWord;

  public FindSpellCheckErrorListener() {
  }

  public void beginChecking(SpellCheckEvent event) {
    this.badWord = null;
  }

  public void spellingError(SpellCheckEvent event) {
    this.createError(event);
  }

  public void badCaseError(SpellCheckEvent event) {
    this.createError(event);
  }

  public void repeatWordError(SpellCheckEvent event) {
    this.createError(event);
  }

  public org.dts.spell.finder.Word getInvalidWord() {
    return this.badWord;
  }

  public boolean hasError() {
    return this.badWord != null;
  }

  private void createError(SpellCheckEvent event) {
    this.badWord = event.getCurrentWord();
    event.cancel();
  }
}
