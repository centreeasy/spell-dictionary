//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.dts.spell;

import org.dts.spell.dictionary.SpellDictionary;
import org.dts.spell.event.ErrorCountListener;
import org.dts.spell.event.FindSpellCheckErrorListener;
import org.dts.spell.event.SpellCheckEvent;
import org.dts.spell.event.SpellCheckListener;
import org.dts.spell.finder.CharSequenceWordFinder;
import org.dts.spell.finder.Word;
import org.dts.spell.finder.WordFinder;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;

public class SpellChecker {
  private HashSet<String> ignore = new HashSet();
  private HashMap<String, String> replace = new HashMap();
  private boolean skipNumbers = true;
  private boolean ignoreUpperCaseWords = true;
  private boolean caseSensitive = true;
  private SpellDictionary dictionary;
  private static final FindSpellCheckErrorListener ERROR_FIND_LISTENER = new FindSpellCheckErrorListener();
  private static final ErrorCountListener ERROR_COUNT_LISTENER = new ErrorCountListener();

  public SpellChecker(SpellDictionary dictionary) {
    this.dictionary = dictionary;
  }

  public void setDictionary(SpellDictionary dictionary) {
    this.dictionary = dictionary;
  }

  public SpellDictionary getDictionary() {
    return this.dictionary;
  }

  public void addIgnore(String word) {
    this.ignore.add(word.trim());
  }

  public void resetIgnore() {
    this.ignore.clear();
  }

  public void setSkipNumbers(boolean skip) {
    this.skipNumbers = skip;
  }

  public boolean isSkipNumbers() {
    return this.skipNumbers;
  }

  public void addReplace(String oldWord, String newWord) {
    this.replace.put(oldWord.trim(), newWord.trim());
  }

  public void resetReplace() {
    this.replace.clear();
  }

  public void setCaseSensitive(boolean sensitive) {
    this.caseSensitive = sensitive;
  }

  public boolean isCaseSensitive() {
    return this.caseSensitive;
  }

  public boolean isIgnoreUpperCaseWords() {
    return this.ignoreUpperCaseWords;
  }

  public void setIgnoreUpperCaseWords(boolean ignore) {
    this.ignoreUpperCaseWords = ignore;
  }

  private boolean checkCase(Word word) {
    return this.isCaseSensitive()?(word.isUpperCase()?(!this.isIgnoreUpperCaseWords()?!this.dictionary.isCorrect(word.getText().toLowerCase()):true):word.isCorrectFirstChar() || !this.dictionary.isCorrect(word.getText().toLowerCase())):true;
  }

  public boolean isCorrect(CharSequence txt) {
    return this.isCorrect((WordFinder)(new CharSequenceWordFinder(txt)));
  }

  public boolean isCorrect(WordFinder finder) {
    return this.check(finder, new FindSpellCheckErrorListener());
  }

  public Word checkSpell(CharSequence txt) {
    return this.checkSpell((WordFinder)(new CharSequenceWordFinder(txt)));
  }

  public Word checkSpell(WordFinder finder) {
    this.check(finder, ERROR_FIND_LISTENER);
    return ERROR_FIND_LISTENER.getInvalidWord();
  }

  public int getErrorCount(CharSequence txt) {
    return this.getErrorCount((WordFinder)(new CharSequenceWordFinder(txt)));
  }

  public int getErrorCount(WordFinder finder) {
    this.check(finder, ERROR_COUNT_LISTENER);
    return ERROR_COUNT_LISTENER.getErrorsCount();
  }

  private boolean isRepeat(Word word, Word last) {
    return last != null && word.getText().equalsIgnoreCase(last.getText()) && !word.isStartOfSentence();
  }

  private boolean isNumber(Word word) {
    try {
      new BigDecimal(word.getText());
      return true;
    } catch (NumberFormatException var3) {
      return false;
    }
  }

  private boolean canSkipWord(Word word) {
    return this.isNumber(word) && this.isSkipNumbers();
  }

  private SpellCheckEvent checkCurrent(Word word, Word last, WordFinder finder, SpellCheckListener listener) {
    String wordText = word.getText();
    String newString = (String)this.replace.get(wordText);
    SpellDictionary dict = this.getDictionary();
    SpellCheckEvent event = null;
    if(newString != null) {
      finder.replace(newString);
    } else if(!this.ignore.contains(wordText)) {
      if(this.isRepeat(word, last)) {
        event = new SpellCheckEvent(this, finder);
        listener.repeatWordError(event);
      } else if(!this.canSkipWord(word)) {
        if(!dict.isCorrect(wordText)) {
          event = new SpellCheckEvent(this, finder);
          listener.spellingError(event);
        } else if(!this.checkCase(word)) {
          event = new SpellCheckEvent(this, finder);
          listener.badCaseError(event);
        }
      }
    }

    return event;
  }

  public boolean check(WordFinder finder, SpellCheckListener listener) {
    boolean result = true;
    Word lastWord = null;
    SpellCheckEvent event = new SpellCheckEvent(this, finder);
    listener.beginChecking(event);

    Word word;
    for(boolean exit = event.isCancel(); !exit && finder.hasNext(); lastWord = word) {
      word = finder.next();
      event = this.checkCurrent(word, lastWord, finder, listener);
      if(event != null) {
        result = false;
        exit = event.isCancel();
      }
    }

    listener.endChecking(new SpellCheckEvent(this, finder));
    return result;
  }
}
