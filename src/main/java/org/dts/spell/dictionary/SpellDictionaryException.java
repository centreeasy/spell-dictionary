//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.dts.spell.dictionary;

public class SpellDictionaryException extends Exception {
  private static final long serialVersionUID = 1L;

  public SpellDictionaryException() {
  }

  public SpellDictionaryException(String message) {
    super(message);
  }

  public SpellDictionaryException(String message, Throwable cause) {
    super(message, cause);
  }

  public SpellDictionaryException(Throwable cause) {
    super(cause);
  }
}
