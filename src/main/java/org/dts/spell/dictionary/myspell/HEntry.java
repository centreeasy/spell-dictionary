//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.dts.spell.dictionary.myspell;

public class HEntry {
  public String word;
  public String astr;

  public HEntry(String word, String astr) {
    this.word = word;
    this.astr = astr;
  }
}
