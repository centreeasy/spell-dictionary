//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.dts.spell.dictionary.myspell;

public class AffEntry {
  String appnd;
  String strip;
  short numconds;
  short xpflg;
  char achar;
  char[] conds;

  public AffEntry() {
    this.conds = new char[Utils.SETSIZE];
  }
}
