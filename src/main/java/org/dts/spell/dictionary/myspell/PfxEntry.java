//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.dts.spell.dictionary.myspell;

public class PfxEntry extends AffEntry {
  AffixMgr pmyMgr;
  PfxEntry next;
  PfxEntry nexteq;
  PfxEntry nextne;
  PfxEntry flgnxt;

  public PfxEntry(AffixMgr pmgr, AffEntry dp) {
    this.pmyMgr = pmgr;
    this.achar = dp.achar;
    this.strip = dp.strip;
    this.appnd = dp.appnd;
    this.numconds = dp.numconds;
    this.xpflg = dp.xpflg;
    System.arraycopy(dp.conds, 0, this.conds, 0, Utils.SETSIZE);
    this.next = null;
    this.nextne = null;
    this.nexteq = null;
  }

  public HEntry check(String word) {
    int len = word.length();
    int appndl = this.appnd.length();
    int stripl = this.strip.length();
    String tmpword = "";
    int tmpl = len - appndl;
    if (tmpl > 0 && tmpl + stripl >= this.numconds) {
      tmpword = this.strip + word.substring(appndl);
      int cp = 0;

      int cond;
      for (cond = 0; cond < this.numconds && (this.conds[tmpword.charAt(cp++)] & 1 << cond) != 0; ++cond) {
        ;
      }

      if (cond >= this.numconds) {
        HEntry he;
        if ((he = this.pmyMgr.lookup(tmpword)) != null && Utils.TestAff(he.astr, this.achar, he.astr.length())) {
          return he;
        }

        if ((this.xpflg & Utils.XPRODUCT) != 0) {
          he = this.pmyMgr.suffix_check(tmpword, Utils.XPRODUCT, this);
          if (he != null) {
            return he;
          }
        }
      }
    }

    return null;
  }

  boolean allowCross() {
    return (this.xpflg & Utils.XPRODUCT) != 0;
  }

  public char getFlag() {
    return this.achar;
  }

  public String getKey() {
    return this.appnd;
  }

  public String add(String word) {
    int len = word.length();
    int stripl = this.strip.length();
    if (len > stripl && len >= this.numconds) {
      int cp = 0;

      int cond;
      for (cond = 0; cond < this.numconds && (this.conds[word.charAt(cp++)] & 1 << cond) != 0; ++cond) {
        ;
      }

      if (cond >= this.numconds) {
        return this.appnd + word.substring(stripl);
      }
    }

    return null;
  }

  public PfxEntry getNext() {
    return this.next;
  }

  public void setNext(PfxEntry ptr) {
    this.next = ptr;
  }

  public PfxEntry getNextNE() {
    return this.nextne;
  }

  public void setNextNE(PfxEntry ptr) {
    this.nextne = ptr;
  }

  public PfxEntry getNextEQ() {
    return this.nexteq;
  }

  public void setNextEQ(PfxEntry ptr) {
    this.nexteq = ptr;
  }

  public PfxEntry getFlgNxt() {
    return this.flgnxt;
  }

  public void setFlgNxt(PfxEntry ptr) {
    this.flgnxt = ptr;
  }
}
