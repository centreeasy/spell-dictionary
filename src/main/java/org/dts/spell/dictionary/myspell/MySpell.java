//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.dts.spell.dictionary.myspell;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

public class MySpell {
  public static final int NOCAP = 0;
  public static final int INITCAP = 1;
  public static final int ALLCAP = 2;
  public static final int HUHCAP = 3;
  private AffixMgr pAMgr;
  private HashMap<String, HEntry> pHMgr;
  private SuggestMgr pSMgr;
  private String encoding;
  private int maxSug;

  public MySpell(String zipFile) throws IOException {
    this(new ZipFile(zipFile));
  }

  public MySpell(ZipFile zipFile) throws IOException {
    Enumeration entries = zipFile.entries();
    InputStream affStream = null;
    InputStream dStream = null;

    while(entries.hasMoreElements() && (affStream == null || dStream == null)) {
      ZipEntry entry = (ZipEntry)entries.nextElement();
      if(entry.getName().endsWith(".aff")) {
        affStream = zipFile.getInputStream(entry);
      } else if(entry.getName().endsWith(".dic")) {
        dStream = zipFile.getInputStream(entry);
      }
    }

    this.initFromStreams(affStream, dStream);
    affStream.close();
    dStream.close();
  }

  public MySpell(InputStream zipStream) throws IOException {
    this(new ZipInputStream(zipStream));
  }

  public MySpell(ZipInputStream zipStream) throws IOException {
    ZipEntry entry = zipStream.getNextEntry();
    Object affStream = null;

    Object dStream;
    for(dStream = null; entry != null; entry = zipStream.getNextEntry()) {
      if(entry.getName().endsWith(".aff")) {
        if(dStream != null) {
          affStream = zipStream;
          break;
        }

        affStream = this.createFromZipEntry(zipStream, entry);
      } else if(entry.getName().endsWith(".dic")) {
        if(affStream != null) {
          dStream = zipStream;
          break;
        }

        dStream = this.createFromZipEntry(zipStream, entry);
      }
    }

    this.initFromStreams((InputStream)affStream, (InputStream)dStream);
  }

  public MySpell(InputStream affStream, InputStream dStream) throws IOException {
    this.initFromStreams(affStream, dStream);
  }

  public MySpell(String affpath, String dpath) throws IOException {
    FileInputStream affStream = null;
    FileInputStream dStream = null;

    try {
      affStream = new FileInputStream(affpath);
      dStream = new FileInputStream(dpath);
      this.initFromStreams(affStream, dStream);
    } finally {
      Utils.close(affStream);
      Utils.close(dStream);
    }

  }

  private InputStream createFromZipEntry(ZipInputStream zipStream, ZipEntry entry) throws IOException {
    int size = (int)entry.getSize();
    byte[] data = new byte[size];
    int cReaded = zipStream.read(data);

    for(int current = cReaded; cReaded > 0; current += cReaded) {
      cReaded = zipStream.read(data, current, size - current);
    }

    return new ByteArrayInputStream(data);
  }

  private void initFromStreams(InputStream affStream, InputStream dStream) throws IOException {
    this.encoding = AffixMgr.readEncoding(affStream);
    this.pHMgr = this.load_tables(dStream);
    this.pAMgr = new AffixMgr(affStream, this.encoding, this.pHMgr);
    String try_string = this.pAMgr.get_try_string();
    this.maxSug = 15;
    this.pSMgr = new SuggestMgr(try_string, this.maxSug, this.pAMgr);
  }

  public List<String> suggest(String word) {
    if(this.pSMgr == null) {
      return Collections.emptyList();
    } else {
      int[] captype = new int[1];
      boolean[] abbv = new boolean[1];
      String cw = this.cleanword(word, captype, abbv);
      int wl = cw.length();
      if(wl == 0) {
        return Collections.emptyList();
      } else {
        Object wlst = new LinkedList();
        String wspace;
        ListIterator it;
        switch(captype[0]) {
          case 0:
            wlst = this.pSMgr.suggest((List)wlst, cw);
            break;
          case 1:
            wspace = cw.toLowerCase();
            this.pSMgr.suggest((List)wlst, wspace);
            it = ((List)wlst).listIterator();

            while(it.hasNext()) {
              it.set(Utils.mkInitCap((String)it.next()));
            }

            this.pSMgr.suggest((List)wlst, cw);
            break;
          case 2:
            wspace = cw.toLowerCase();
            this.pSMgr.suggest((List)wlst, wspace);
            it = ((List)wlst).listIterator();

            while(it.hasNext()) {
              it.set(((String)it.next()).toUpperCase());
            }

            this.pSMgr.suggest((List)wlst, cw);
            break;
          case 3:
            this.pSMgr.suggest((List)wlst, cw);
            wspace = cw.toLowerCase();
            this.pSMgr.suggest((List)wlst, wspace);
        }

        return (List)(!((List)wlst).isEmpty()?wlst:wlst);
      }
    }
  }

  public boolean spell(String word) {
    String rv = null;

    try {
      int[] captype = new int[1];
      boolean[] abbv = new boolean[1];
      String cw = this.cleanword(word, captype, abbv);
      int wl = cw.length();
      if(wl == 0) {
        return true;
      }

      String wspace;
      switch(captype[0]) {
        case 0:
        case 3:
          rv = this.check(cw);
          if(abbv[0] && rv == null) {
            cw = cw + '.';
            rv = this.check(cw);
          }
          break;
        case 1:
          wspace = cw.toLowerCase();
          rv = this.check(wspace);
          if(rv == null) {
            rv = this.check(cw);
          }

          if(abbv[0] && rv == null) {
            wspace = cw + '.';
            rv = this.check(wspace);
          }
          break;
        case 2:
          wspace = cw.toLowerCase();
          rv = this.check(wspace);
          if(rv == null) {
            rv = this.check(Utils.mkInitCap(wspace));
          }

          if(rv == null) {
            rv = this.check(cw);
          }

          if(abbv[0] && rv == null) {
            wspace = cw + '.';
            rv = this.check(wspace);
          }
      }
    } catch (Exception var8) {
      ;
    }

    return rv != null;
  }

  public String get_dic_encoding() {
    return this.encoding;
  }

  private HashMap<String, HEntry> load_tables(InputStream tStream) throws IOException {
    HashMap result = null;
    BufferedReader rawdict = null;

    try {
      rawdict = new BufferedReader(new InputStreamReader(tStream, this.encoding));
      String ts = rawdict.readLine();
      if(ts == null) {
        throw new IOException(Utils.getString("ERROR_HASH_MANAGER_2"));
      }

      int tablesize = Integer.parseInt(ts);
      if(tablesize == 0) {
        throw new IOException(Utils.getString("ERROR_HASH_MANAGER_4"));
      }

      HEntry en;
      for(result = new HashMap(tablesize); (ts = rawdict.readLine()) != null; result.put(en.word, en)) {
        ts = ts.trim();
        int ap = ts.indexOf(47);
        if(ap != -1) {
          en = new HEntry(ts.substring(0, ap), ts.substring(ap + 1));
        } else {
          en = new HEntry(ts, "");
        }
      }
    } finally {
      Utils.close(rawdict);
    }

    return result;
  }

  private String cleanword(String src, int[] pcaptype, boolean[] pabbrev) {
    boolean p = false;

    int q;
    for(q = 0; q < src.length() && !Character.isLetterOrDigit(src.charAt(q)); ++q) {
      ;
    }

    pabbrev[0] = false;

    int nl;
    for(nl = src.substring(q).length(); nl > 0 && !Character.isLetterOrDigit(src.charAt(q + nl - 1)); --nl) {
      ;
    }

    if(q + nl < src.length() && src.charAt(q + nl) == 46) {
      pabbrev[0] = true;
    }

    if(nl <= 0) {
      pcaptype[0] = 0;
      pabbrev[0] = false;
      return "";
    } else {
      int ncap = 0;
      int nneutral = 0;

      int nc;
      for(nc = 0; nl > 0; --nl) {
        ++nc;
        char c = src.charAt(q);
        if(Character.isUpperCase(c)) {
          ++ncap;
        }

        if(!Character.isUpperCase(c) && !Character.isLowerCase(c)) {
          ++nneutral;
        }

        ++q;
      }

      if(ncap == 0) {
        pcaptype[0] = 0;
      } else if(ncap == 1 && Character.isUpperCase(src.charAt(q))) {
        pcaptype[0] = 1;
      } else if(ncap != nc && ncap + nneutral != nc) {
        pcaptype[0] = 3;
      } else {
        pcaptype[0] = 2;
      }

      return src.substring(q, q);
    }
  }

  private String check(String word) {
    HEntry he = null;
    if(this.pHMgr != null) {
      he = (HEntry)this.pHMgr.get(word);
    }

    if(he == null && this.pAMgr != null) {
      he = this.pAMgr.affix_check(word);
      if(he == null && this.pAMgr.get_compound() != null) {
        he = this.pAMgr.compound_check(word, this.pAMgr.get_compound().charAt(0));
      }
    }

    return he != null?he.word:null;
  }

  public void addCustomWord(String word) {
    HEntry en = new HEntry(word, "");
    this.pHMgr.put(en.word, en);
  }
}
