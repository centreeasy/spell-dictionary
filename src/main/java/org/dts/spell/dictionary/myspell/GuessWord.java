//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.dts.spell.dictionary.myspell;

public class GuessWord {
  String word;
  boolean allow;

  GuessWord(String word, boolean allow) {
    this.word = word;
    this.allow = allow;
  }
}
