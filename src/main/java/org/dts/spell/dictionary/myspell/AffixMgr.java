//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.dts.spell.dictionary.myspell;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

public class AffixMgr {
  private AffEntry[] pStart;
  private AffEntry[] sStart;
  private AffEntry[] pFlag;
  private AffEntry[] sFlag;
  private HashMap<String, HEntry> pHMgr;
  private String trystring;
  private String encoding;
  private String compound;
  private int cpdmin;
  private ReplEntry[] reptable;
  private MapEntry[] maptable;
  private boolean nosplitsugs;

  public AffixMgr(InputStream affStream, String encoding, HashMap<String, HEntry> ptr) throws IOException {
    this.pStart = new AffEntry[Utils.SETSIZE];
    this.sStart = new AffEntry[Utils.SETSIZE];
    this.pFlag = new AffEntry[Utils.SETSIZE];
    this.sFlag = new AffEntry[Utils.SETSIZE];
    this.trystring = null;
    this.encoding = null;
    this.compound = null;
    this.cpdmin = 3;
    this.reptable = null;
    this.maptable = null;
    this.nosplitsugs = false;
    this.pHMgr = ptr;
    this.parse_file(affStream, encoding);
  }

  public HEntry affix_check(String word) {
    HEntry rv = null;
    rv = this.prefix_check(word);
    if(rv != null) {
      return rv;
    } else {
      rv = this.suffix_check(word, 0, (AffEntry)null);
      return rv;
    }
  }

  public HEntry prefix_check(String word) {
    HEntry rv = null;

    for(PfxEntry pe = (PfxEntry)this.pStart[0]; pe != null; pe = pe.getNext()) {
      rv = pe.check(word);
      if(rv != null) {
        return rv;
      }
    }

    byte sp = 0;
    PfxEntry pptr = (PfxEntry)this.pStart[word.charAt(sp)];

    while(pptr != null) {
      if(Utils.isSubset(pptr.getKey(), word)) {
        rv = pptr.check(word);
        if(rv != null) {
          return rv;
        }

        pptr = pptr.getNextEQ();
      } else {
        pptr = pptr.getNextNE();
      }
    }

    return null;
  }

  public HEntry suffix_check(String word, int sfxopts, AffEntry ppfx) {
    HEntry rv = null;

    for(SfxEntry se = (SfxEntry)this.sStart[0]; se != null; se = se.getNext()) {
      rv = se.check(word, sfxopts, ppfx);
      if(rv != null) {
        return rv;
      }
    }

    String tmpword = Utils.myRevStrDup(word);
    char sp = tmpword.charAt(0);
    SfxEntry sptr = (SfxEntry)this.sStart[sp];

    while(sptr != null) {
      if(Utils.isSubset(sptr.getKey(), tmpword)) {
        rv = sptr.check(word, sfxopts, ppfx);
        if(rv != null) {
          return rv;
        }

        sptr = sptr.getNextEQ();
      } else {
        sptr = sptr.getNextNE();
      }
    }

    return null;
  }

  public List<GuessWord> expand_rootword(String ts, String ap) {
    int al = ap.length();
    LinkedList wlst = new LinkedList();
    wlst.add(new GuessWord(ts, false));

    int n;
    for(n = 0; n < al; ++n) {
      char it = ap.charAt(n);

      for(SfxEntry m = (SfxEntry)this.sFlag[it]; m != null; m = m.getFlgNxt()) {
        String c = m.add(ts);
        if(c != null) {
          wlst.add(new GuessWord(c, m.allowCross()));
        }
      }
    }

    n = wlst.size();
    Iterator var13 = wlst.iterator();
    var13.next();

    int var14;
    for(var14 = 1; var14 < n; ++var14) {
      GuessWord var15 = (GuessWord)var13.next();
      if(var15.allow) {
        for(int ptr = 0; ptr < al; ++ptr) {
          char newword = ap.charAt(ptr);

          for(PfxEntry cptr = (PfxEntry)this.pFlag[newword]; cptr != null; cptr = cptr.getFlgNxt()) {
            if(cptr.allowCross()) {
              String newword1 = cptr.add(var15.word);
              if(newword1 != null) {
                wlst.add(new GuessWord(newword1, cptr.allowCross()));
              }
            }
          }
        }
      }
    }

    for(var14 = 0; var14 < al; ++var14) {
      char var16 = ap.charAt(var14);

      for(PfxEntry var17 = (PfxEntry)this.pFlag[var16]; var17 != null; var17 = var17.getFlgNxt()) {
        String var18 = var17.add(ts);
        if(var18 != null) {
          wlst.add(new GuessWord(var18, var17.allowCross()));
        }
      }
    }

    return wlst;
  }

  public HEntry compound_check(String word, char compound_flag) {
    int len = word.length();
    HEntry rv = null;
    if(len < this.cpdmin) {
      return null;
    } else {
      for(int i = this.cpdmin; i < len - (this.cpdmin - 1); ++i) {
        String st = word.substring(0, i);
        rv = this.lookup(st);
        if(rv == null) {
          rv = this.affix_check(st);
        }

        if(rv != null && Utils.TestAff(rv.astr, compound_flag, rv.astr.length())) {
          String wordI = word.substring(i, word.length());
          rv = this.lookup(wordI);
          if(rv != null && Utils.TestAff(rv.astr, compound_flag, rv.astr.length())) {
            return rv;
          }

          rv = this.affix_check(wordI);
          if(rv != null && Utils.TestAff(rv.astr, compound_flag, rv.astr.length())) {
            return rv;
          }

          rv = this.compound_check(wordI, compound_flag);
          if(rv != null) {
            return rv;
          }
        }
      }

      return null;
    }
  }

  public HEntry lookup(String word) {
    return this.pHMgr == null?null:(HEntry)this.pHMgr.get(word);
  }

  public int get_numrep() {
    return this.reptable != null?this.reptable.length:0;
  }

  public ReplEntry[] get_reptable() {
    return this.reptable;
  }

  public int get_nummap() {
    return this.maptable != null?this.maptable.length:0;
  }

  public MapEntry[] get_maptable() {
    return this.maptable;
  }

  public String get_encoding() {
    if(this.encoding == null) {
      this.encoding = "ISO8859-1";
    }

    return this.encoding;
  }

  public String get_try_string() {
    return this.trystring;
  }

  public String get_compound() {
    return this.compound;
  }

  public boolean get_nosplitsugs() {
    return this.nosplitsugs;
  }

  public static String readEncoding(String affpath) throws IOException {
    FileInputStream rd = null;

    String var3;
    try {
      rd = new FileInputStream(affpath);
      var3 = readEncoding((InputStream)rd);
    } finally {
      Utils.close(rd);
    }

    return var3;
  }

  public static String readEncoding(InputStream affStream) throws IOException {
    StringBuilder builder = new StringBuilder(20);

    for(int r = affStream.read(); -1 != r && 10 != r; r = affStream.read()) {
      builder.append((char)r);
    }

    return parseEncoding(builder.toString());
  }

  private void parse_file(InputStream affStream, String encoding) throws IOException {
    BufferedReader rd = new BufferedReader(new InputStreamReader(affStream, encoding));
    this.encoding = encoding;

    String line;
    while((line = rd.readLine()) != null) {
      if(line.startsWith("PFX")) {
        this.parse_affix(line, 'P', rd);
      } else if(line.startsWith("SFX")) {
        this.parse_affix(line, 'S', rd);
      } else if(line.startsWith("TRY")) {
        this.parse_try(line);
      } else if(line.startsWith("SET")) {
        this.parse_set(line);
      } else if(line.startsWith("COMPOUNDFLAG")) {
        this.parse_cpdflag(line);
      } else if(line.startsWith("COMPOUNDMIN")) {
        this.parse_cpdmin(line);
      } else if(line.startsWith("REP")) {
        this.parse_reptable(line, rd);
      } else if(line.startsWith("MAP")) {
        this.parse_maptable(line, rd);
      } else if(line.startsWith("NOSPLITSUGS")) {
        this.nosplitsugs = true;
      }
    }

    this.process_pfx_order();
    this.process_sfx_order();
  }

  private void parse_try(String line) throws IOException {
    if(this.trystring != null) {
      throw new IOException(Utils.getString("ERROR_DUPLICATE_TRY"));
    } else {
      StringTokenizer tp = new StringTokenizer(line, " ");
      int i = 0;
      int np = 0;

      while(tp.hasMoreTokens()) {
        String piece = tp.nextToken();
        if(piece.length() != 0) {
          switch(i) {
            case 0:
              ++np;
              break;
            case 1:
              this.trystring = piece;
              ++np;
          }

          ++i;
        }
      }

      if(np != 2) {
        throw new IOException(Utils.getString("ERROR_MISSING_TRY"));
      }
    }
  }

  private static String parseEncoding(String line) throws IOException {
    if(line == null) {
      throw new IOException(Utils.getString("ERROR_MISSING_SET"));
    } else {
      StringTokenizer tp = new StringTokenizer(line, " ");
      int i = 0;
      int np = 0;
      String result = null;

      while(tp.hasMoreTokens()) {
        String piece = tp.nextToken();
        if(piece.length() != 0) {
          switch(i) {
            case 0:
              ++np;
              break;
            case 1:
              result = piece;
              ++np;
          }

          ++i;
        }
      }

      if(np != 2) {
        throw new IOException(Utils.getString("ERROR_MISSING_SET"));
      } else {
        return result;
      }
    }
  }

  private void parse_set(String line) throws IOException {
    if(this.encoding != null) {
      throw new IOException(Utils.getString("ERROR_DUPLICATE_SET"));
    } else {
      this.encoding = parseEncoding(line);
    }
  }

  private void parse_cpdflag(String line) throws IOException {
    if(this.compound != null) {
      throw new IOException(Utils.getString("ERROR_DUPLICATE_COMPOUND_FLAGS"));
    } else {
      StringTokenizer tp = new StringTokenizer(line, " ");
      int i = 0;
      int np = 0;

      while(tp.hasMoreTokens()) {
        String piece = tp.nextToken();
        if(piece.length() != 0) {
          switch(i) {
            case 0:
              ++np;
              break;
            case 1:
              this.compound = piece;
              ++np;
          }

          ++i;
        }
      }

      if(np != 2) {
        throw new IOException(Utils.getString("ERROR_MISSING_COMPOUND_FLAG"));
      }
    }
  }

  private void parse_cpdmin(String line) throws IOException {
    StringTokenizer tp = new StringTokenizer(line, " ");
    int i = 0;
    int np = 0;

    while(tp.hasMoreTokens()) {
      String piece = tp.nextToken();
      if(piece.length() != 0) {
        switch(i) {
          case 0:
            ++np;
            break;
          case 1:
            this.cpdmin = Integer.parseInt(piece);
            ++np;
        }

        ++i;
      }
    }

    if(np != 2) {
      throw new IOException(Utils.getString("ERROR_MISSING_COMPOUND_MIN"));
    } else {
      if(this.cpdmin < 1 || this.cpdmin > 50) {
        this.cpdmin = 3;
      }

    }
  }

  private void parse_reptable(String line, BufferedReader af) throws IOException {
    int numrep = this.get_numrep();
    if(numrep != 0) {
      throw new IOException(Utils.getString("ERROR_DUPLICATE_REP"));
    } else {
      StringTokenizer tp = new StringTokenizer(line, " ");
      int i = 0;
      int np = 0;

      String piece;
      while(tp.hasMoreTokens()) {
        piece = tp.nextToken();
        if(piece.length() != 0) {
          switch(i) {
            case 0:
              ++np;
              break;
            case 1:
              numrep = Integer.parseInt(piece);
              if(numrep < 1) {
                throw new IOException(Utils.getString("INCORRECT_NUMBER_OF_ENTRIES_REP_TABLE"));
              }

              this.reptable = new ReplEntry[numrep];
              ++np;
          }

          ++i;
        }
      }

      if(np != 2) {
        throw new IOException(Utils.getString("ERROR_MISSING_REP_TABLE"));
      } else {
        for(int j = 0; j < numrep; ++j) {
          tp = new StringTokenizer(af.readLine(), " ");
          i = 0;
          this.reptable[j] = new ReplEntry();
          this.reptable[j].pattern = null;
          this.reptable[j].replacement = null;

          while(tp.hasMoreTokens()) {
            piece = tp.nextToken();
            if(piece.length() != 0) {
              switch(i) {
                case 0:
                  if(!piece.startsWith("REP")) {
                    throw new IOException(Utils.getString("ERROR_REP_TABLE_CORRUPT"));
                  }
                  break;
                case 1:
                  this.reptable[j].pattern = piece;
                  break;
                case 2:
                  this.reptable[j].replacement = piece;
              }

              ++i;
            }
          }

          if(this.reptable[j].pattern == null || this.reptable[j].replacement == null) {
            throw new IOException(Utils.getString("ERROR_REP_TABLE_CORRUPT"));
          }
        }

      }
    }
  }

  private void parse_maptable(String line, BufferedReader af) throws IOException {
    int nummap = this.get_nummap();
    if(nummap != 0) {
      throw new IOException(Utils.getString("ERROR_DUPLICATE_MAP"));
    } else {
      StringTokenizer tp = new StringTokenizer(line, " ");
      int i = 0;
      int np = 0;

      String piece;
      while(tp.hasMoreTokens()) {
        piece = tp.nextToken();
        if(piece.length() != 0) {
          switch(i) {
            case 0:
              ++np;
              break;
            case 1:
              nummap = Integer.parseInt(piece);
              if(nummap < 1) {
                throw new IOException(Utils.getString("ERROR_NUMBER_ENTRIES_MAP"));
              }

              this.maptable = new MapEntry[nummap];
              ++np;
          }

          ++i;
        }
      }

      if(np != 2) {
        throw new IOException(Utils.getString("ERROR_MISSING_MAP"));
      } else {
        for(int j = 0; j < nummap; ++j) {
          tp = new StringTokenizer(line, " ");
          i = 0;
          this.maptable[j] = new MapEntry();
          this.maptable[j].set = null;

          while(tp.hasMoreTokens()) {
            piece = tp.nextToken();
            if(piece.length() != 0) {
              switch(i) {
                case 0:
                  if(!piece.startsWith("MAP")) {
                    throw new IOException(Utils.getString("ERROR_MAP_CORRUPT"));
                  }
                  break;
                case 1:
                  this.maptable[j].set = piece;
              }

              ++i;
            }
          }

          if(this.maptable[j].set == null || this.maptable[j].set.length() == 0) {
            throw new IOException(Utils.getString("ERROR_MAP_CORRUPT"));
          }
        }

      }
    }
  }

  private void parse_affix(String line, char at, BufferedReader af) throws IOException {
    int numents = 0;
    char achar = 0;
    short ff = 0;
    AffEntry[] ptr = (AffEntry[])null;
    boolean nptr = false;
    StringTokenizer tp = new StringTokenizer(line, " ");
    int i = 0;
    int np = 0;

    while(true) {
      String piece;
      int k;
      do {
        if(!tp.hasMoreTokens()) {
          if(np != 4) {
            MessageFormat var18 = new MessageFormat(Utils.getString("ERROR_AFFIX_HEADER"));
            throw new IOException(var18.format(new Object[]{new Character(achar), line}));
          }

          int var16 = 0;

          for(k = 0; k < numents; ++k) {
            String nl = af.readLine();
            tp = new StringTokenizer(nl, " ");
            i = 0;
            np = 0;

            MessageFormat sfxptr;
            while(tp.hasMoreTokens()) {
              piece = tp.nextToken();
              if(piece.length() != 0) {
                switch(i) {
                  case 0:
                    ++np;
                    if(var16 != 0) {
                      ptr[var16].xpflg = ptr[0].xpflg;
                    }
                    break;
                  case 1:
                    ++np;
                    if(piece.charAt(0) != achar) {
                      sfxptr = new MessageFormat(Utils.getString("ERROR_AFFIX_HEADER_CORRUPT_COUNT"));
                      throw new IOException(sfxptr.format(new Object[]{new Character(achar), nl}));
                    }

                    if(var16 != 0) {
                      ptr[var16].achar = ptr[0].achar;
                    }
                    break;
                  case 2:
                    ++np;
                    ptr[var16].strip = piece;
                    if(ptr[var16].strip.equals("0")) {
                      ptr[var16].strip = "";
                    }
                    break;
                  case 3:
                    ++np;
                    ptr[var16].appnd = piece;
                    if(ptr[var16].appnd.equals("0")) {
                      ptr[var16].appnd = "";
                    }
                    break;
                  case 4:
                    ++np;
                    this.encodeit(ptr[var16], piece);
                }

                ++i;
              }
            }

            if(np != 5) {
              sfxptr = new MessageFormat(Utils.getString("ERROR_AFFIX_HEADER_CORRUPT"));
              throw new IOException(sfxptr.format(new Object[]{new Character(achar), nl}));
            }

            ++var16;
          }

          var16 = 0;

          for(k = 0; k < numents; ++k) {
            if(at == 80) {
              PfxEntry var17 = new PfxEntry(this, ptr[var16]);
              this.build_pfxlist(var17);
            } else {
              SfxEntry var19 = new SfxEntry(this, ptr[var16]);
              this.build_sfxlist(var19);
            }

            ++var16;
          }

          return;
        }

        piece = tp.nextToken();
      } while(piece.length() == 0);

      switch(i) {
        case 0:
          ++np;
          break;
        case 1:
          ++np;
          achar = piece.charAt(0);
          break;
        case 2:
          ++np;
          if(piece.charAt(0) == 89) {
            ff = (short)Utils.XPRODUCT;
          }
          break;
        case 3:
          ++np;
          numents = Integer.parseInt(piece);
          ptr = new AffEntry[numents];

          for(k = 0; k < numents; ++k) {
            ptr[k] = new AffEntry();
          }

          ptr[0].xpflg = ff;
          ptr[0].achar = achar;
      }

      ++i;
    }
  }

  private void encodeit(AffEntry ptr, String cs) {
    char[] mbr = new char[Utils.MAXLNLEN];

    int i;
    for(i = 0; i < Utils.SETSIZE; ++i) {
      ptr.conds[i] = 0;
    }

    int nc = cs.length();
    boolean neg = false;
    boolean grp = false;
    int n = 0;
    boolean ec = false;
    int nm = 0;
    if(cs.equals(".")) {
      ptr.numconds = 0;
    } else {
      for(i = 0; i < nc; ++i) {
        char c = cs.charAt(i);
        if(c == 91) {
          grp = true;
          c = 0;
        }

        if(grp && c == 94) {
          neg = true;
          c = 0;
        }

        if(c == 93) {
          ec = true;
          c = 0;
        }

        if(grp && c != 0) {
          mbr[nm] = c;
          ++nm;
          c = 0;
        }

        if(c != 0) {
          ec = true;
        }

        if(ec) {
          int j;
          if(grp) {
            char k;
            if(!neg) {
              for(j = 0; j < nm; ++j) {
                k = mbr[j];
                ptr.conds[k] = (char)(ptr.conds[k] | 1 << n);
              }
            } else {
              for(j = 0; j < Utils.SETSIZE; ++j) {
                ptr.conds[j] = (char)(ptr.conds[j] | 1 << n);
              }

              for(j = 0; j < nm; ++j) {
                k = mbr[j];
                ptr.conds[k] = (char)(ptr.conds[k] & ~(1 << n));
              }
            }

            neg = false;
            grp = false;
            nm = 0;
          } else if(c == 46) {
            for(j = 0; j < Utils.SETSIZE; ++j) {
              ptr.conds[j] = (char)(ptr.conds[j] | 1 << n);
            }
          } else {
            ptr.conds[c] = (char)(ptr.conds[c] | 1 << n);
          }

          ++n;
          ec = false;
        }
      }

      ptr.numconds = (short)n;
    }
  }

  private void build_pfxlist(AffEntry pfxptr) {
    PfxEntry ep = (PfxEntry)pfxptr;
    String key = ep.getKey();
    char flg = ep.getFlag();
    PfxEntry ptr = (PfxEntry)this.pFlag[flg];
    ep.setFlgNxt(ptr);
    this.pFlag[flg] = ep;
    if(key.length() == 0) {
      ptr = (PfxEntry)this.pStart[0];
      ep.setNext(ptr);
      this.pStart[0] = ep;
    } else {
      char sp = key.charAt(0);
      ptr = (PfxEntry)this.pStart[sp];
      if(ptr != null && ep.getKey().compareTo(ptr.getKey()) > 0) {
        PfxEntry pptr;
        for(pptr = null; ptr != null && ep.getKey().compareTo(ptr.getKey()) > 0; ptr = ptr.getNext()) {
          pptr = ptr;
        }

        pptr.setNext(ep);
        ep.setNext(ptr);
      } else {
        ep.setNext(ptr);
        this.pStart[sp] = ep;
      }
    }
  }

  private void build_sfxlist(AffEntry sfxptr) {
    SfxEntry ep = (SfxEntry)sfxptr;
    String key = ep.getKey();
    char flg = ep.getFlag();
    SfxEntry ptr = (SfxEntry)this.sFlag[flg];
    ep.setFlgNxt(ptr);
    this.sFlag[flg] = ep;
    if(key.length() == 0) {
      ptr = (SfxEntry)this.sStart[0];
      ep.setNext(ptr);
      this.sStart[0] = ep;
    } else {
      char sp = key.charAt(0);
      ptr = (SfxEntry)this.sStart[sp];
      if(ptr != null && ep.getKey().compareTo(ptr.getKey()) > 0) {
        SfxEntry pptr;
        for(pptr = null; ptr != null && ep.getKey().compareTo(ptr.getKey()) > 0; ptr = ptr.getNext()) {
          pptr = ptr;
        }

        pptr.setNext(ep);
        ep.setNext(ptr);
      } else {
        ep.setNext(ptr);
        this.sStart[sp] = ep;
      }
    }
  }

  private void process_pfx_order() {
    for(int i = 1; i < Utils.SETSIZE; ++i) {
      PfxEntry ptr;
      PfxEntry nptr;
      for(ptr = (PfxEntry)this.pStart[i]; ptr != null; ptr = ptr.getNext()) {
        for(nptr = ptr.getNext(); nptr != null && Utils.isSubset(ptr.getKey(), nptr.getKey()); nptr = nptr.getNext()) {
          ;
        }

        ptr.setNextNE(nptr);
        ptr.setNextEQ((PfxEntry)null);
        if(ptr.getNext() != null && Utils.isSubset(ptr.getKey(), ptr.getNext().getKey())) {
          ptr.setNextEQ(ptr.getNext());
        }
      }

      for(ptr = (PfxEntry)this.pStart[i]; ptr != null; ptr = ptr.getNext()) {
        nptr = ptr.getNext();

        PfxEntry mptr;
        for(mptr = null; nptr != null && Utils.isSubset(ptr.getKey(), nptr.getKey()); nptr = nptr.getNext()) {
          mptr = nptr;
        }

        if(mptr != null) {
          mptr.setNextNE((PfxEntry)null);
        }
      }
    }

  }

  private void process_sfx_order() {
    for(int i = 1; i < Utils.SETSIZE; ++i) {
      SfxEntry ptr;
      SfxEntry nptr;
      for(ptr = (SfxEntry)this.sStart[i]; ptr != null; ptr = ptr.getNext()) {
        for(nptr = ptr.getNext(); nptr != null && Utils.isSubset(ptr.getKey(), nptr.getKey()); nptr = nptr.getNext()) {
          ;
        }

        ptr.setNextNE(nptr);
        ptr.setNextEQ((SfxEntry)null);
        if(ptr.getNext() != null && Utils.isSubset(ptr.getKey(), ptr.getNext().getKey())) {
          ptr.setNextEQ(ptr.getNext());
        }
      }

      for(ptr = (SfxEntry)this.sStart[i]; ptr != null; ptr = ptr.getNext()) {
        nptr = ptr.getNext();

        SfxEntry mptr;
        for(mptr = null; nptr != null && Utils.isSubset(ptr.getKey(), nptr.getKey()); nptr = nptr.getNext()) {
          mptr = nptr;
        }

        if(mptr != null) {
          mptr.setNextNE((SfxEntry)null);
        }
      }
    }

  }
}
