//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.dts.spell.dictionary.myspell;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class SuggestMgr {
  private static final int MAX_ROOTS = 10;
  private static final int MAX_GUESS = 10;
  private static final int NGRAM_LONGER_WORSE = 1;
  private static final int NGRAM_ANY_MISMATCH = 2;
  private static final char[] EMPTY_TRY = new char[0];
  private char[] ctry;
  private AffixMgr pAMgr;
  private int maxSug;
  private boolean nosplitsugs;

  public SuggestMgr(String tryme, int maxn, AffixMgr aptr) {
    this.pAMgr = aptr;
    if (tryme != null) {
      this.ctry = tryme.toCharArray();
    } else {
      this.ctry = EMPTY_TRY;
    }

    this.maxSug = maxn;
    this.nosplitsugs = false;
    if (this.pAMgr != null) {
      this.nosplitsugs = this.pAMgr.get_nosplitsugs();
    }

  }

  public List<String> suggest(List<String> wlst, String word) {
    int nsug = wlst.size();
    if (nsug < this.maxSug) {
      nsug = this.mapchars(wlst, word);
    }

    if (nsug < this.maxSug) {
      nsug = this.replchars(wlst, word);
    }

    if (nsug < this.maxSug) {
      nsug = this.forgotchar(wlst, word);
    }

    if (nsug < this.maxSug) {
      nsug = this.swapchar(wlst, word);
    }

    if (nsug < this.maxSug) {
      nsug = this.extrachar(wlst, word);
    }

    if (nsug < this.maxSug) {
      nsug = this.badchar(wlst, word);
    }

    if (!this.nosplitsugs && nsug < this.maxSug) {
      this.twowords(wlst, word);
    }

    return wlst;
  }

  public boolean check(String word) {
    boolean result = true;

    try {
      HEntry rv = null;
      if (this.pAMgr != null) {
        rv = this.pAMgr.lookup(word);
        if (rv == null) {
          rv = this.pAMgr.affix_check(word);
        }
      }

      result = rv != null;
    } catch (Exception var4) {
      ;
    }

    return result;
  }

  public List<String> ngsuggest(List<String> wlst, String word, HashMap<String, HEntry> pHMgr) {
    if (pHMgr == null) {
      return wlst;
    } else {
      HEntry[] roots = new HEntry[10];
      int[] scores = new int[10];

      int i;
      for (i = 0; i < 10; ++i) {
        scores[i] = -100 * i;
      }

      int lp = 9;
      int n = word.length();
      Iterator it = pHMgr.values().iterator();

      while (true) {
        int j;
        int sc;
        HEntry thresh;
        do {
          if (!it.hasNext()) {
            int var21 = 0;
            char[] var22 = (char[]) null;

            for (int guess = 1; guess < 4; ++guess) {
              var22 = word.toCharArray();

              for (int gscore = guess; gscore < n; gscore += 4) {
                var22[gscore] = 42;
              }

              var21 += this.ngram(n, word, new String(var22), 2);
              var22 = (char[]) null;
            }

            var22 = (char[]) null;
            var21 /= 3;
            --var21;
            String[] var23 = new String[10];
            int[] var24 = new int[10];

            for (i = 0; i < 10; ++i) {
              var24[i] = -100 * i;
            }

            lp = 9;

            label101:
            for (i = 0; i < 10; ++i) {
              if (roots[i] != null) {
                HEntry unique = roots[i];
                List glst = this.pAMgr.expand_rootword(unique.word, unique.astr);
                Iterator itGuess = glst.iterator();

                while (true) {
                  GuessWord glstK;
                  do {
                    do {
                      if (!itGuess.hasNext()) {
                        continue label101;
                      }

                      glstK = (GuessWord) itGuess.next();
                      sc = this.ngram(n, word, glstK.word, 2);
                    } while (sc <= var21);
                  } while (sc <= var24[lp]);

                  var24[lp] = sc;
                  var23[lp] = glstK.word;
                  int lval = sc;

                  for (j = 0; j < 10; ++j) {
                    if (var24[j] < lval) {
                      lp = j;
                      lval = var24[j];
                    }
                  }
                }
              }
            }

            this.bubblesort(var23, var24);

            for (i = 0; i < 10; ++i) {
              if (var23[i] != null) {
                boolean var25 = true;

                for (j = i + 1; j < 10 && var25; ++j) {
                  var25 = !var23[i].equals(var23[j]);
                }

                if (var25) {
                  wlst.add(var23[i]);
                }
              }
            }

            return wlst;
          }

          thresh = (HEntry) it.next();
          sc = this.ngram(3, word, thresh.word, 1);
        } while (sc <= scores[lp]);

        scores[lp] = sc;
        roots[lp] = thresh;
        int mw = sc;

        for (j = 0; j < 10; ++j) {
          if (scores[j] < mw) {
            lp = j;
            mw = scores[j];
          }
        }
      }
    }
  }

  private int replchars(List<String> wlst, String word) {
    int ns = wlst.size();
    int wl = word.length();
    if (wl >= 2 && this.pAMgr != null) {
      int numrep = this.pAMgr.get_numrep();
      ReplEntry[] reptable = this.pAMgr.get_reptable();
      if (reptable == null) {
        return ns;
      } else {
        for (int i = 0; i < numrep; ++i) {
          int r = 0;

          for (int lenp = reptable[i].pattern.length(); (r = word.indexOf(reptable[i].pattern, r)) != -1; ++r) {
            String candidate = word.substring(0, r) + reptable[i].replacement + word.substring(r + lenp);
            if (!wlst.contains(candidate) && this.check(candidate)) {
              if (ns >= this.maxSug) {
                return wlst.size();
              }

              wlst.add(candidate);
            }
          }
        }

        return wlst.size();
      }
    } else {
      return ns;
    }
  }

  private int mapchars(List<String> wlst, String word) {
    int ns = wlst.size();
    int wl = word.length();
    if (wl >= 2 && this.pAMgr != null) {
      MapEntry[] maptable = this.pAMgr.get_maptable();
      if (maptable == null) {
        return ns;
      } else {
        this.map_related(word, 0, wlst, maptable);
        return wlst.size();
      }
    } else {
      return ns;
    }
  }

  private void map_related(String word, int i, List<String> wlst, MapEntry[] maptable) {
    int nummap = maptable.length;
    char c = word.charAt(i);
    if (word.length() <= i) {
      if (!wlst.contains(word) && this.check(word) && wlst.size() < this.maxSug) {
        wlst.add(word);
      }

    } else {
      boolean in_map = false;

      for (int j = 0; j < nummap && wlst.size() < this.maxSug; ++j) {
        if (maptable[j].set.indexOf(c) != -1) {
          in_map = true;
          StringBuilder newword = new StringBuilder(word);

          for (int k = 0; k < maptable[j].set.length() && wlst.size() < this.maxSug; ++k) {
            newword.setCharAt(i, maptable[j].set.charAt(k));
            this.map_related(newword.toString(), i + 1, wlst, maptable);
          }
        }
      }

      if (!in_map) {
        ++i;
        this.map_related(word, i, wlst, maptable);
      }

    }
  }

  private int forgotchar(List<String> wlst, String word) {
    char[] candidate = new char[word.length() + 1];
    int wl = word.length();
    int ctryl = this.ctry.length;
    System.arraycopy(word.toCharArray(), 0, candidate, 1, wl);

    int q;
    int i;
    String candidateStr;
    int ns;
    for (q = 0; q < wl; ++q) {
      for (i = 0; i < ctryl; ++i) {
        candidate[q] = this.ctry[i];
        candidateStr = new String(candidate);
        if (!wlst.contains(candidateStr) && this.check(candidateStr)) {
          ns = wlst.size();
          if (ns >= this.maxSug) {
            return ns;
          }

          wlst.add(candidateStr);
        }
      }

      candidate[q] = word.charAt(q);
    }

    for (i = 0; i < ctryl; ++i) {
      candidate[q] = this.ctry[i];
      candidateStr = new String(candidate);
      if (!wlst.contains(candidateStr) && this.check(candidateStr)) {
        ns = wlst.size();
        if (ns >= this.maxSug) {
          return ns;
        }

        wlst.add(candidateStr);
      }
    }

    return wlst.size();
  }

  private int swapchar(List<String> wlst, String word) {
    int wl = word.length();
    char[] candidate = word.toCharArray();

    for (int p = 0; p + 1 < wl; ++p) {
      char tmpc = candidate[p];
      candidate[p] = candidate[p + 1];
      candidate[p + 1] = tmpc;
      String candidateStr = new String(candidate);
      if (!wlst.contains(candidateStr) && this.check(candidateStr)) {
        int ns = wlst.size();
        if (ns >= this.maxSug) {
          return ns;
        }

        wlst.add(candidateStr);
      }

      tmpc = candidate[p];
      candidate[p] = candidate[p + 1];
      candidate[p + 1] = tmpc;
    }

    return wlst.size();
  }

  private int extrachar(List<String> wlst, String word) {
    int wl = word.length();
    if (wl < 2) {
      return wlst.size();
    } else {
      char[] candidate = new char[wl];
      System.arraycopy(word.toCharArray(), 1, candidate, 0, wl - 1);

      for (int p = 0; p < wl; ++p) {
        String candidateStr = new String(candidate, 0, wl - 1);
        if (!wlst.contains(candidateStr) && this.check(candidateStr)) {
          int ns = wlst.size();
          if (ns >= this.maxSug) {
            return ns;
          }

          wlst.add(candidateStr);
        }

        candidate[p] = word.charAt(p);
      }

      return wlst.size();
    }
  }

  private int badchar(List<String> wlst, String word) {
    char[] candidate = word.toCharArray();
    int wl = word.length();
    int ctryl = this.ctry.length;

    for (int i = 0; i < wl; ++i) {
      char tmpc = candidate[i];

      for (int j = 0; j < ctryl; ++j) {
        if (this.ctry[j] != tmpc) {
          candidate[i] = this.ctry[j];
          String candidateStr = new String(candidate);
          if (!wlst.contains(candidateStr) && this.check(candidateStr)) {
            int ns = wlst.size();
            if (ns >= this.maxSug) {
              return ns;
            }

            wlst.add(candidateStr);
          }

          candidate[i] = tmpc;
        }
      }
    }

    return wlst.size();
  }

  private int twowords(List<String> wlst, String word) {
    int wl = word.length();
    if (wl < 3) {
      return wlst.size();
    } else {
      char[] candidate = new char[wl + 1];
      System.arraycopy(word.toCharArray(), 0, candidate, 1, wl);

      for (int p = 1; p + 1 < wl + 1; ++p) {
        candidate[p - 1] = candidate[p];
        String candidateStr = new String(candidate, 0, p);
        if (this.check(candidateStr)) {
          candidateStr = new String(candidate, p + 1, candidate.length - (p + 1));
          if (this.check(candidateStr)) {
            int ns = wlst.size();
            candidate[p] = 32;
            if (ns >= this.maxSug) {
              return ns;
            }

            wlst.add(new String(candidate));
          }
        }
      }

      return wlst.size();
    }
  }

  private int ngram(int n, String s1, String s2, int uselen) {
    int nscore = 0;
    int l1 = s1.length();
    int l2 = s2.length();

    int ns;
    for (int j = 1; j <= n; ++j) {
      ns = 0;

      for (int i = 0; i <= l1 - j; ++i) {
        if (s2.indexOf(s1.substring(i, i + j)) != -1) {
          ++ns;
        }
      }

      nscore += ns;
      if (ns < 2) {
        break;
      }
    }

    ns = 0;
    if (uselen == 1) {
      ns = l2 - l1 - 2;
    }

    if (uselen == 2) {
      ns = Math.abs(l2 - l1) - 2;
    }

    return nscore - (ns > 0 ? ns : 0);
  }

  private void bubblesort(String[] rword, int[] rsc) {
    int n = rword.length;

    for (int m = 1; m < n; ++m) {
      for (int j = m; j > 0 && rsc[j - 1] < rsc[j]; --j) {
        int sctmp = rsc[j - 1];
        String wdtmp = rword[j - 1];
        rsc[j - 1] = rsc[j];
        rword[j - 1] = rword[j];
        rsc[j] = sctmp;
        rword[j] = wdtmp;
      }
    }

  }
}
