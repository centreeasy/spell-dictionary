//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.dts.spell.dictionary.myspell;

public class SfxEntry extends AffEntry {
  AffixMgr pmyMgr;
  String rappnd;
  SfxEntry next;
  SfxEntry nexteq;
  SfxEntry nextne;
  SfxEntry flgnxt;

  public SfxEntry(AffixMgr pmgr, AffEntry dp) {
    this.pmyMgr = pmgr;
    this.achar = dp.achar;
    this.strip = dp.strip;
    this.appnd = dp.appnd;
    this.numconds = dp.numconds;
    this.xpflg = dp.xpflg;
    System.arraycopy(dp.conds, 0, this.conds, 0, Utils.SETSIZE);
    this.rappnd = Utils.myRevStrDup(this.appnd);
  }

  public HEntry check(String word, int optflags, AffEntry ppfx) {
    int len = word.length();
    int appndl = this.appnd.length();
    int stripl = this.strip.length();
    PfxEntry ep = (PfxEntry) ppfx;
    if ((optflags & Utils.XPRODUCT) != 0 && (this.xpflg & Utils.XPRODUCT) == 0) {
      return null;
    } else {
      int tmpl = len - appndl;
      if (tmpl > 0 && tmpl + stripl >= this.numconds) {
        int cp = tmpl + stripl;
        String tmpword = word.substring(0, tmpl) + this.strip;
        int cond = this.numconds;

        do {
          --cond;
          if (cond < 0) {
            break;
          }

          --cp;
        } while ((this.conds[tmpword.charAt(cp)] & 1 << cond) != 0);

        HEntry he;
        if (cond < 0 && (he = this.pmyMgr.lookup(tmpword)) != null && Utils.TestAff(he.astr, this.achar, he.astr.length()) && ((optflags & Utils.XPRODUCT) == 0 || Utils.TestAff(he.astr, ep.getFlag(), he.astr.length()))) {
          return he;
        }
      }

      return null;
    }
  }

  public boolean allowCross() {
    return (this.xpflg & Utils.XPRODUCT) != 0;
  }

  public char getFlag() {
    return this.achar;
  }

  public String getKey() {
    return this.rappnd;
  }

  public String add(String word) {
    int len = word.length();
    int stripl = this.strip.length();
    if (len > stripl && len >= this.numconds) {
      int cp = len;
      int cond = this.numconds;

      do {
        --cond;
        if (cond < 0) {
          break;
        }

        --cp;
      } while ((this.conds[word.charAt(cp)] & 1 << cond) != 0);

      if (cond < 0) {
        return word.substring(0, len - stripl) + this.appnd;
      }
    }

    return null;
  }

  public SfxEntry getNext() {
    return this.next;
  }

  public void setNext(SfxEntry ptr) {
    this.next = ptr;
  }

  public SfxEntry getNextNE() {
    return this.nextne;
  }

  public void setNextNE(SfxEntry ptr) {
    this.nextne = ptr;
  }

  public SfxEntry getNextEQ() {
    return this.nexteq;
  }

  public void setNextEQ(SfxEntry ptr) {
    this.nexteq = ptr;
  }

  public SfxEntry getFlgNxt() {
    return this.flgnxt;
  }

  public void setFlgNxt(SfxEntry ptr) {
    this.flgnxt = ptr;
  }
}
