//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.dts.spell.dictionary;

import org.dts.spell.dictionary.myspell.MySpell;
import org.dts.spell.dictionary.myspell.Utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.zip.ZipFile;

public class OpenOfficeSpellDictionary implements SpellDictionary {
  private File personalDict;
  private MySpell mySpell;
  private boolean isLoaded;
  private Future loader;

  public OpenOfficeSpellDictionary(ZipFile zipFile) throws IOException {
    this(zipFile, true);
  }

  public OpenOfficeSpellDictionary(final ZipFile zipFile, boolean inBackground) throws IOException {
    this.isLoaded = false;
    this.loader = null;
    if (inBackground) {
      final ExecutorService executor = Executors.newSingleThreadExecutor();
      this.loader = executor.submit(new Callable() {
        public Object call() throws Exception {
          OpenOfficeSpellDictionary.this.initFromZipFile(zipFile);
          executor.shutdown();
          return null;
        }
      });
    } else {
      this.initFromZipFile(zipFile);
    }

  }

  public OpenOfficeSpellDictionary(InputStream zipStream, File personalDict) throws IOException {
    this(zipStream, personalDict, true);
  }

  public OpenOfficeSpellDictionary(final InputStream zipStream, final File personalDict, boolean inBackground) throws IOException {
    this.isLoaded = false;
    this.loader = null;
    if (inBackground) {
      final ExecutorService executor = Executors.newSingleThreadExecutor();
      this.loader = executor.submit(new Callable() {
        public Object call() throws Exception {
          OpenOfficeSpellDictionary.this.initFromStream(zipStream, personalDict);
          zipStream.close();
          executor.shutdown();
          return null;
        }
      });
    } else {
      this.initFromStream(zipStream, personalDict);
    }

  }

  public OpenOfficeSpellDictionary(File dictFile, File affFile) throws IOException {
    this(dictFile, affFile, true);
  }

  public OpenOfficeSpellDictionary(final File dictFile, final File affFile, boolean inBackground) throws IOException {
    this.isLoaded = false;
    this.loader = null;
    if (inBackground) {
      final ExecutorService executor = Executors.newSingleThreadExecutor();
      this.loader = executor.submit(new Callable() {
        public Object call() throws Exception {
          OpenOfficeSpellDictionary.this.initFromFiles(dictFile, affFile);
          executor.shutdown();
          return null;
        }
      });
    } else {
      this.initFromFiles(dictFile, affFile);
    }

  }

  private File extractRootFile(File file) {
    String name = file.getName();
    int index = name.lastIndexOf(46);
    String rootName;
    if (index != -1) {
      rootName = name.substring(0, index);
    } else {
      rootName = name;
    }

    return new File(file.getParent(), rootName);
  }

  private void initFromFiles(File dictFile, File affFile) throws IOException {
    this.personalDict = this.getPersonalWordsFile(this.extractRootFile(dictFile));
    this.mySpell = new MySpell(affFile.getPath(), dictFile.getPath());
    this.readPersonalWords(this.personalDict);
    this.notifyLoaded();
  }

  private void initFromZipFile(ZipFile zipFile) throws IOException {
    this.personalDict = this.getPersonalWordsFile(this.extractRootFile(new File(zipFile.getName())));
    this.mySpell = new MySpell(zipFile);
    this.readPersonalWords(this.personalDict);
    this.notifyLoaded();
  }

  private void initFromStream(InputStream zipStream, File personalDict) throws IOException {
    this.personalDict = personalDict;
    this.mySpell = new MySpell(zipStream);
    this.readPersonalWords(personalDict);
    this.notifyLoaded();
  }

  public void addWord(String word) throws SpellDictionaryException {
    this.waitToLoad();
    PrintWriter pw = null;
    word = word.trim();

    try {
      pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(this.personalDict, true), this.mySpell.get_dic_encoding()));
      this.mySpell.addCustomWord(word);
      pw.println(word);
    } catch (Exception var11) {
      throw new SpellDictionaryException(var11);
    } finally {
      try {
        Utils.close(pw);
      } catch (IOException var10) {
        throw new SpellDictionaryException(var10);
      }
    }

  }

  public boolean isCorrect(String word) {
    this.waitToLoad();
    return this.mySpell.spell(word);
  }

  public List<String> getSuggestions(String word) {
    this.waitToLoad();
    return this.mySpell.suggest(word);
  }

  private synchronized void waitToLoad() {
    try {
      if (!this.isLoaded) {
        if (this.loader != null && this.loader.isDone()) {
          this.loader.get();
        } else {
          this.wait();
        }
      }

    } catch (Exception var2) {
      throw new IllegalStateException(var2);
    }
  }

  private synchronized void notifyLoaded() {
    this.isLoaded = true;
    this.notify();
  }

  private File getPersonalWordsFile(File rootFile) {
    return new File(rootFile.getParent(), rootFile.getName() + ".per");
  }

  private void readPersonalWords(File personalFile) throws IOException {
    BufferedReader rd = null;

    try {
      if (personalFile != null && personalFile.exists() && !personalFile.isDirectory()) {
        rd = new BufferedReader(new InputStreamReader(new FileInputStream(personalFile), this.mySpell.get_dic_encoding()));

        for (String line = rd.readLine(); line != null; line = rd.readLine()) {
          this.mySpell.addCustomWord(line.trim());
        }
      }
    } finally {
      Utils.close(rd);
    }

  }
}
