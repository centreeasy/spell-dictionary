//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.dts.spell.dictionary;

import java.util.List;

public interface SpellDictionary {
  void addWord(String var1) throws SpellDictionaryException;

  boolean isCorrect(String var1);

  List<String> getSuggestions(String var1);
}
